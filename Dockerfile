FROM registry.gitlab.com/comiq/robotframework-container:snapshot

USER root

RUN pip install robotframework-pabot

COPY pabot.sh /usr/local/bin/
RUN chmod ugo+x /usr/local/bin/pabot.sh
USER robotframework
ENTRYPOINT ["/usr/local/bin/pabot.sh"]
CMD ["--help"]
